# davek1312/validation

Bootstraps illuminate/validation package.

# Installation

The package is available on [Packagist](https://packagist.org/packages/davek1312/validation),
you can install it using [Composer](https://getcomposer.org/).

```bash
composer require davek1312/validation
```

# Configuration

Copy the `vendor/davek1312/validation/davek1312` folder to your application's root directory.

# Language Files
The default validation messages can be viewed at `vendor/davek1312/validation/lang/en/validation.php`.

## Locale Language Files
To set up different locale language files for validation view the [Translation Package](https://packagist.org/packages/davek1312/translation) documentation.

## Overriding Validation Messages
To override validation messages add a custom `validation.php` to `davek1312/translation/lang/vendor/validation/{locale}/`.

#Rules
To view the available validation rules view the [Laravel Documentation](https://laravel.com/docs/validation#available-validation-rules).

#Usage
```php
<?php 

use Davek1312\Validation\Validator;

$data = [
    'key' => 'value'
];
$rules = [
    'key' => 'required'
];
$validator = new Validator($data, $rules, [], $locale = 'en');

//Check if the validation passes
$validator->passes();

//Check if the validation fails
$validator->failed();

//Get the errors
$validator->errors();
```

#Custom Error Messages
If needed, you may use custom error messages for validation instead of the defaults. You can define customer messages either programmatically or in the language files.

##Programmatically
```php
//This will be the message for the required rule
$customValidationMessages = [
    'required' => 'The :attribute field is required.',
];
$validator = new Validator($data, $rules, $customValidationMessages);

//This will be the message for the required rule only for the `email` attribute
$customValidationMessages = [
    'email.required' => 'We need to know your e-mail address!',
];
$validator = new Validator($data, $rules, $customValidationMessages);
```

## In Language Files
Add your customer messages to the `custom` array in your `davek1312/translation/lang/vendor/validation/{locale}/validation.php` file.
```php
'custom' => [
    'email' => [
         //This will be the message for the required rule only for the `email` attribute
        'required' => 'We need to know your e-mail address!',
    ],
],
```

If you would like the :attribute portion of your validation message to be replaced with a custom attribute name, you may specify the custom name in the attributes array of your `davek1312/translation/lang/vendor/validation/{locale}/validation.php` file.
```php
'attributes' => [
    'email' => 'email address',
],
```

#Custom Validation Rules
To register custom validation rules view the [App Package](https://packagist.org/packages/davek1312/app) documentation.

```php
 protected function register() {
    // Will NOT run if the value being validated is empty
    $this->registerValidationRules('foo', 'Class@validateFoo');
    
    // Will run if the value being validated is empty
    $this->registerImplicitValidationRules('foo', 'Class@validateFoo');
    
    // Custom place-holder replacements for error messages
    $this->registerValidationReplacers('foo', 'Class@replaceFoo');
 }
```

##Custom Validation Messages
You will also need to define an error message for your custom rule. You can do so either using an inline custom message array or by adding an entry in the validation language file. This message should be placed in the first level of the array, not within the custom array, which is only for attribute-specific error messages:
```php
"foo" => "Your input was invalid!",
```