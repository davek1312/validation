<?php

namespace Davek1312\Validation;

use Davek1312\App\App;
use Davek1312\Translation\Translator;

/**
 * Wrapper for Illuminate\Validation\Validator
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
class Validator extends \Illuminate\Validation\Validator {

    /**
     * @var App
     */
    private $app;

    /**
     * Create a new Validator instance.
     *
     * @param array  $data
     * @param array  $rules
     * @param array $customValidationMessages
     * @param string $locale
     * @param App $app
     */
    public function __construct(array $data, array $rules, array $customValidationMessages = [], $locale = null, App $app = null) {
        $this->generateApp($app);
        $this->translator = $this->generateTranslator($locale);
        parent::__construct($this->translator, $data, $rules, $this->getValidationLang(), $this->generateCustomValidationAttributes());
        $this->addCustomValidationMessages($customValidationMessages);
        $this->addCustomerValidationRules();
    }

    /**
     * @param App $app
     *
     * @return void
     */
    private function generateApp(App $app = null) {
        $this->app = $app ? $app : new App();
    }

    /**
     * @param string $locale
     *
     * @return Translator
     */
    private function generateTranslator($locale) {
        $translator = new Translator();
        if($locale) {
            $translator->setLocale($locale);
        }
        return $translator;
    }

    /**
     * @param string $key
     *
     * @return array
     */
    private function getValidationLang($key = '') {
        $baseKey = 'validation::validation';
        $key = $key ? "{$baseKey}.{$key}" : $baseKey;
        return $this->translator->get($key);
    }

    /**
     * @return array
     */
    private function generateCustomValidationAttributes() {
        $customValidationAttributes = $this->getValidationLang('attributes');
        return is_array($customValidationAttributes) ? $customValidationAttributes : [];
    }

    /**
     * @return array
     */
    private function generateCustomValidationMessages() {
        $customValidationMessages = $this->getValidationLang('custom');
        return is_array($customValidationMessages) ? $customValidationMessages : [];
    }

    /**
     * @param array $customValidationMessages
     *
     * @return void
     */
    private function addCustomValidationMessages(array $customValidationMessages) {
        $this->setCustomMessages($this->generateCustomValidationMessages());
        $this->setCustomMessages($customValidationMessages);
    }

    /**
     * @return void
     */
    private function addCustomerValidationRules() {
        $this->addExtensions($this->app->getValidationRules());
        $this->addImplicitExtensions($this->app->getImplicitValidationRules());
        $this->addReplacers($this->app->getValidationReplacers());
    }

    /**
     * @return App
     */
    public function getApp() {
        return $this->app;
    }

    /**
     * @param App $app
     */
    public function setApp($app) {
        $this->app = $app;
    }
}