<?php

namespace Davek1312\Validation;

use Davek1312\App\App;
use Davek1312\Config\Config;

/**
 * Registers davek1312 components
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
class Registry extends \Davek1312\App\Registry {

    /**
     * Register the database configuration file
     *
     * @return void
     */
    protected function register() {
        $this->registerLang('validation', __DIR__.'/../lang');
    }
}