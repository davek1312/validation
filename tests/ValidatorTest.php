<?php

namespace Davek1312\Validation\Tests;

use Davek1312\App\App;
use Davek1312\Validation\Validator;

class ValidatorTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var array
     */
    private $data;
    /**
     * @var array
     */
    private $rules;
    /**
     * @var Validator
     */
    private $validator;
    /**
     * @var array
     */
    private $customValidationMessages;
    /**
     * @var App
     */
    private $app;

    public function setUp() {
        parent::setUp();
        $this->data = [
            'test' => null,
        ];
        $this->rules = [
            'test' => 'required'
        ];
        $this->customValidationMessages = [
            'custom' => 'message',
        ];
        $this->app = $this->getApp();
        $this->validator = new Validator($this->data, $this->rules, $this->customValidationMessages, 'en', $this->app);
    }

    private function getApp() {
        $app = new App();
        $app->setValidationRules([
            'custom-rule' => 'Davek1312\Validation\Tests\ValidatorTest@customValidationRule'
        ]);
        $app->setImplicitValidationRules([
            'custom-rule-implicit' => 'Davek1312\Validation\Tests\ValidatorTest@customValidationRuleImplicit'
        ]);
        $app->setValidationReplacers([
            'custom-rule' => 'Davek1312\Validation\Tests\ValidatorTest@customValidationReplacer'
        ]);
        return $app;
    }

    public function test__construct() {
        $this->assertGenerateApp();
        $this->assertGenerateTranslator();
        $this->assertEquals($this->data, $this->validator->getData());
        $this->assertEquals([
            'test' => ['required']
        ], $this->validator->getRules());
        $this->assertGetValidationLangAndAddCustomValidationMessages();
        $this->assertEquals([], $this->validator->customAttributes);
        $this->assertAddCustomerValidationRules();
    }

    private function assertGenerateApp() {
        $this->assertEquals($this->app, $this->validator->getApp());

        $validator = new Validator([], [], []);
        $this->assertInstanceOf(App::class, $validator->getApp());
    }

    private function assertGenerateTranslator() {
        $this->assertEquals('en', $this->validator->getTranslator()->getLocale());
    }

    private function assertGetValidationLangAndAddCustomValidationMessages() {
        $this->assertEquals($this->getMessages(), $this->validator->customMessages);
    }

    private function getMessages() {
        $validationLang = include __DIR__.'/../lang/en/validation.php';
        return array_merge($validationLang, $validationLang['custom'], $this->customValidationMessages);
    }

    public function testValidatorWorks() {
        $this->assertFalse($this->validator->passes());
        $this->assertEquals('The test field is required.', $this->validator->errors()->first());
    }

    private function assertAddCustomerValidationRules() {
        $this->assertEquals([
            'custom-rule' => 'Davek1312\Validation\Tests\ValidatorTest@customValidationRule',
            'custom-rule-implicit' => 'Davek1312\Validation\Tests\ValidatorTest@customValidationRuleImplicit',
        ], $this->validator->extensions);
        $this->assertEquals([
            'custom-rule' => 'Davek1312\Validation\Tests\ValidatorTest@customValidationReplacer',
        ], $this->validator->replacers);
    }
}