<?php

namespace Davek1312\Validation\Tests;

use Davek1312\Validation\Registry;

class RegistryTest extends \PHPUnit_Framework_TestCase {

    private $registry;

    public function setUp() {
        parent::setUp();
        $this->registry = new Registry();
    }

    public function testLangRegistered() {
        $lang = $this->registry->getLang();
        $this->assertContains('src/../lang', $lang['validation']);
    }
}